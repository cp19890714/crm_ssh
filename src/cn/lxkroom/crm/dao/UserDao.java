package cn.lxkroom.crm.dao;

import cn.lxkroom.crm.domain.User;

public interface UserDao extends BaseDao<User> {
	
	User getByUserPwd(User user);
}
