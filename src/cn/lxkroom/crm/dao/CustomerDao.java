package cn.lxkroom.crm.dao;

import cn.lxkroom.crm.domain.Customer;

public interface CustomerDao extends BaseDao<Customer> {

}
