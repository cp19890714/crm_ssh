package cn.lxkroom.crm.dao.Impl;

import java.util.List;

import org.springframework.orm.hibernate5.HibernateTemplate;

import cn.lxkroom.crm.dao.UserDao;
import cn.lxkroom.crm.domain.User;

public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {
	
	@Override
	public User getByUserPwd(User user) {
		List<User> find = (List<User>) getHibernateTemplate().find("from User u where u.user_code=? and u.user_password=?",
				user.getUser_code(),user.getUser_password());
		if(find.size()>0){
			return find.get(0);
		}
		return null;
		
	}

}
