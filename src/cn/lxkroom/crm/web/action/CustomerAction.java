package cn.lxkroom.crm.web.action;

import java.io.File;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.lxkroom.crm.domain.Customer;
import cn.lxkroom.crm.service.CustomerService;
import cn.lxkroom.crm.utils.PageBean;

public class CustomerAction extends ActionSupport implements ModelDriven<Customer> {
	private static final long serialVersionUID = 1L;
	private Customer customer=new Customer();
	private CustomerService customerService;
	private Integer currentPage;
	private Integer pageSize;
	
	@Override
	public Customer getModel() {
		return customer;
	}

	//分页列表
	public String list() throws Exception {
		//System.out.println(currentPage);
		//封装DC
		DetachedCriteria dc=DetachedCriteria.forClass(Customer.class);
		//参数不为空,封装查询参数
		if(StringUtils.isNoneBlank(customer.getCust_name())){
			dc.add(Restrictions.like("cust_name", "%"+customer.getCust_name()+"%"));
			
		}
		//调用service获取pageBean
		PageBean pb = customerService.getPageBean(dc,pageSize,currentPage);
		//将pageBean放入域中
		ActionContext.getContext().put("pageBean", pb);
		//转发到页面
		return "list";
	}
	//添加客户  加入图片上传功能
	private File image;  // 准备与文件上传组件提交键相同的属性.struts2会自动把文件内容封装到属性中
	private String imageFileName;  //接收上传文件名称,只需要准备 "文件提交键名"+"FileName"属性即可
	private String imageContextType;  //接收文件上传类型,同上
	//增加或者修改客户
	public String add() throws Exception {
		customerService.save(customer);
		if(image!=null){
			image.renameTo(new File("f:/upload/"+UUID.randomUUID()+imageFileName+".jpg"));			
		}
		//重定向到列表
		return "toList";
	}
	
	//修改客户
	public String edit() throws Exception {
		Customer c = customerService.getById(customer.getCust_id());
		ActionContext.getContext().put("customer", c);
		return "add";
	}
	
	//删除客户
	public String delete() throws Exception {
		customerService.delete(customer.getCust_id());
		return "toList";
	}
	public Integer getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public CustomerService getCustomerService() {
		return customerService;
	}
	public void setCustomerService(CustomerService customerService) {
		this.customerService = customerService;
	}

	public File getImage() {
		return image;
	}

	public void setImage(File image) {
		this.image = image;
	}

	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getImageContextType() {
		return imageContextType;
	}

	public void setImageContextType(String imageContextType) {
		this.imageContextType = imageContextType;
	}
	
	
}
