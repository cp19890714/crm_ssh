package cn.lxkroom.crm.web.action;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import cn.lxkroom.crm.domain.Linkman;
import cn.lxkroom.crm.domain.User;
import cn.lxkroom.crm.service.UserService;
import cn.lxkroom.crm.utils.PageBean;

public class UserAction extends ActionSupport implements ModelDriven<User> {
	private Integer page;//当前页数
	private Integer rows;//每页显示条数
	@Override
	public User getModel() {
		return user;
	}
	private User user=new User();
	private UserService userService;
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	/*
	 * 用户登录功能	
	 */
	public String login() throws Exception {
		
		User existUser = userService.login(user);
		if(existUser!=null){
			ActionContext.getContext().getSession().put("user", existUser);
			ActionContext.getContext().put("msg", "登录成功");
			return SUCCESS;
		}else{
			ActionContext.getContext().put("msg", "登录失败,用户名与密码不匹配");
			return LOGIN;
		}
	}
	
	/**
	 * 用户注册功能
	 */
	public String regist(){
		int i =  userService.regist(user);
		if(i>0){
			return LOGIN;
		}else{
			return ERROR;
		}
		
	}
	
	public String list() throws Exception {
		
		return null;
	}
	
}
