package cn.lxkroom.crm.utils;

import java.util.List;

public class PageBean {
	//列表数据
	private List list;  //查dao
	//总记录数
	private Integer totalCount;  //查dao
	//每页显示条数
	private Integer pageSize;  //页面参数
	//总页数
	private Integer totalPage;  //计算
	//当前页数
	private Integer currentPage;  //页面参数
	//构造方法
	public PageBean(Integer totalCount, Integer pageSize, Integer currentPage) {
		super();
		this.totalCount = totalCount;
		this.pageSize = pageSize;
		this.currentPage = currentPage;
		
		//判断参数是否为空
		if(this.pageSize == null){
			this.pageSize = 3;
		}
		if(this.currentPage == null){
			this.currentPage= 1;
		}
		//判断参数的有效性
		if(this.currentPage < 1){
			this.currentPage = 1;
		}
		//计算总页数1
		this.totalPage =(this.totalCount%this.pageSize==0)?(this.totalCount/this.pageSize):(this.totalCount/this.pageSize +1);
		//计算总页数
		//方案2
			/*Double  totalPageD = Math.ceil(this.totalCount/this.pageSize);
			this.totalPage = Integer.parseInt(totalPageD+"");*/
		//方案3
			//this.totalPage = (this.totalCount+this.pageSize-1)/this.pageSize;
		
		if(this.currentPage>this.totalPage){
			this.currentPage=this.totalPage;
		}
		
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	public Integer getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	
	//获取开始索引
	public Integer getStartIndex(){
		return (this.currentPage - 1) * this.pageSize;
	}
}
