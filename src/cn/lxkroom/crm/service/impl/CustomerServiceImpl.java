package cn.lxkroom.crm.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.lxkroom.crm.dao.CustomerDao;
import cn.lxkroom.crm.domain.Customer;
import cn.lxkroom.crm.service.CustomerService;
import cn.lxkroom.crm.utils.PageBean;

public class CustomerServiceImpl implements CustomerService {
	private CustomerDao customerDao;
	@Override
	public PageBean getPageBean(DetachedCriteria dc, Integer pageSize, Integer currentPage) {
		//调用dao查询总记录数
		Integer totalCount = customerDao.getTotalCount(dc);
		//创建pageBean对象
		PageBean pb = new PageBean(totalCount, pageSize, currentPage);
		//查询列表,放入pb中
		List<Customer>  list = customerDao.getPageList(dc,pb.getStartIndex(),pb.getPageSize());
		pb.setList(list);
		return pb;
	}
	@Override
	@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=false,propagation=Propagation.REQUIRED)
	public void save(Customer customer) {
		customerDao.save(customer);
		
	}
	
	public void setCustomerDao(CustomerDao customerDao) {
		this.customerDao = customerDao;
	}
	@Override
	public Customer getById(Long cust_id) {
		return customerDao.getById(cust_id);
	}
	@Override
	@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=false,propagation=Propagation.REQUIRED)
	public void delete(Long cust_id) {
		customerDao.delete(cust_id);
		
	}

}
