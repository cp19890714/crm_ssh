package cn.lxkroom.crm.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import cn.lxkroom.crm.dao.UserDao;
import cn.lxkroom.crm.domain.User;
import cn.lxkroom.crm.service.UserService;
import cn.lxkroom.crm.utils.MD5Util;
import cn.lxkroom.crm.utils.PageBean;
@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=true,propagation=Propagation.REQUIRED)
public class UserServiceImpl implements UserService {
	private String name;
	private int age;
	private UserDao ud;
	public void setUd(UserDao ud) {
		this.ud = ud;
	}
	@Override
	@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=false,propagation=Propagation.REQUIRED)
	public void save(User u) {
		ud.save(u);
	}
	@Override
	/**
	 * 用户登录
	 */
	@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=true,propagation=Propagation.REQUIRED)
	public User login(User user) {
		user.setUser_password(MD5Util.md5(user.getUser_password()));
		return ud.getByUserPwd(user);
	}
	@Override
	/**
	 * 用户注册,加密密码
	 */
	@Transactional(isolation=Isolation.REPEATABLE_READ,readOnly=false,propagation=Propagation.REQUIRED)
	public int regist(User user) {
		user.setUser_password(MD5Util.md5(user.getUser_password()));
		user.setUser_state("1");
		ud.save(user);
		return 0;
	}
	@Override
	public PageBean getPageList(DetachedCriteria dc, Integer page, Integer rows) {
		return null;
		
	}

}
