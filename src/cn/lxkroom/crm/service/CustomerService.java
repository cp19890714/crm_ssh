package cn.lxkroom.crm.service;

import org.hibernate.criterion.DetachedCriteria;

import cn.lxkroom.crm.domain.Customer;
import cn.lxkroom.crm.utils.PageBean;

public interface CustomerService {

	PageBean getPageBean(DetachedCriteria dc, Integer pageSize, Integer currentPage);

	void save(Customer customer);

	Customer getById(Long cust_id);

	void delete(Long cust_id);

}
