package cn.lxkroom.crm.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import cn.lxkroom.crm.domain.User;
import cn.lxkroom.crm.utils.PageBean;

public interface UserService {
	void save(User u);

	User login(User user);

	int regist(User user);

	PageBean getPageList(DetachedCriteria dc, Integer page, Integer rows);
}
