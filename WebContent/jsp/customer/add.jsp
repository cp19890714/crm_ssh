﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>${customer==null?'添加':'修改'}客户</TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK href="${pageContext.request.contextPath }/css/Style.css" type=text/css rel=stylesheet>
<LINK href="${pageContext.request.contextPath }/css/Manage.css" type=text/css
	rel=stylesheet>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.8.3.js"></script>
<script type="text/javascript">
	//页面加载后,三个选择框的加载方法     要提交三个参数
	
	//参数一:select提交的建的名称     参数二:select放置到页面哪个元素中    参数三:加载的数据字典类型   四:希望哪个option被选中
	function prepareSelect(name,positionId,typeCode,selectedId){
		//1.创建select对象
		var $select=$("<select name='"+name+"'><option value=''>--请选择--</option></select>");
		//2.发送ajax请求后台数据json,根据项目类别代码查询所有项 
		$.post("${pageContext.request.contextPath}/baseDictAction", {"dict_type_code": typeCode},
				   function(json){
		//3.遍历json,选择的字典ID与
					   $.each(json, function(i,obj){
						   var item_name=obj.dict_item_name;
						   var dict_id=obj.dict_id;
						   //创建option 
						   if(selectedId==dict_id){
							  
							   var $option=$("<option selected value='"+dict_id+"'>"+item_name+"</option>");
						   }else{
							   var $option=$("<option value='"+dict_id+"'>"+item_name+"</option>");
						   }
						   //添加option到select中
						   $($select).append($option);
					   });
				   }, "json");
		//4.把select放置到指定位置
		$("#"+positionId).append($select);
	}
	
	$(function() {
		  prepareSelect("cust_level.dict_id","cust_level","006","${customer.cust_level.dict_id}");
		  prepareSelect("cust_industry.dict_id","cust_industry","001","${customer.cust_industry.dict_id}");
		  prepareSelect("cust_source.dict_id","cust_source","002","${customer.cust_source.dict_id}");
	});
</script>

<META content="MSHTML 6.00.2900.3492" name=GENERATOR>
</HEAD>
<BODY>
	<FORM id="form1" name="form1" action="${pageContext.request.contextPath }/customerAction_add.action" 
											enctype="multipart/form-data" method="post">
		<!-- 回显id的隐藏域 -->
		<input type="hidden" name="cust_id" value="${customer.cust_id}"/>
		<TABLE cellSpacing=0 cellPadding=0 width="98%" border=0>
			<TBODY>
				<TR>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_019.jpg"
						border=0></TD>
					<TD width="100%" background="${pageContext.request.contextPath }/images/new_020.jpg"
						height=20></TD>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_021.jpg"
						border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
		<TABLE cellSpacing=0 cellPadding=0 width="98%" border=0>
			<TBODY>
				<TR>
					<TD width=15 background=${pageContext.request.contextPath }/images/new_022.jpg><IMG
						src="${pageContext.request.contextPath }/images/new_022.jpg" border=0></TD>
					<TD vAlign=top width="100%" bgColor=#ffffff>
						<TABLE cellSpacing=0 cellPadding=5 width="100%" border=0>
							<TR>
								<TD class=manageHead>当前位置：客户管理 &gt; ${customer==null?"添加":"修改"}客户</TD>
							</TR>
							<TR>
								<TD height=2></TD>
							</TR>
						</TABLE>
						
						<TABLE cellSpacing=0 cellPadding=5  border=0>
						  
						    
							<TR>
								<td>客户名称：</td>
								<td>
								<INPUT class=textbox id=sChannel2 value="${customer.cust_name }"
														style="WIDTH: 180px" maxLength=50 name="cust_name">
								</td>
								<td>客户级别 ：</td>
								<td id="cust_level"></td>
							</TR>
							
							<TR>
								<td>信息来源 ：</td>
								<td id="cust_source"></td>
								<td>客户行业：</td>
								<td id="cust_industry"></td>
							</TR>
							
							<TR>
								
								
								<td>固定电话 ：</td>
								<td>
								<INPUT class=textbox id=sChannel2 value="${customer.cust_phone }"
														style="WIDTH: 180px" maxLength=50 name="cust_phone">
								</td>
								<td>移动电话 ：</td>
								<td>
								<INPUT class=textbox id=sChannel2 value="${customer.cust_mobile }"
														style="WIDTH: 180px" maxLength=50 name="cust_mobile">
								</td>
								
								<td>用户照片 ：</td>
								<td>
								<INPUT class=textbox id=sChannel2 type="file"
										style="WIDTH: 180px" maxLength=50 name="image"/>
								</td>
							</TR>
														
							<tr>
								<td rowspan=2>
								<INPUT class=button id=sButton2 type="submit"
														value=" 保存 " name=sButton2>
								</td>
							</tr>
						</TABLE>
						
						
					</TD>
					<TD width=15 background="${pageContext.request.contextPath }/images/new_023.jpg">
					<IMG src="${pageContext.request.contextPath }/images/new_023.jpg" border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
		<TABLE cellSpacing=0 cellPadding=0 width="98%" border=0>
			<TBODY>
				<TR>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_024.jpg"
						border=0></TD>
					<TD align="center" width="100%"
						background="${pageContext.request.contextPath }/images/new_025.jpg" height=15></TD>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_026.jpg"
						border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
	</FORM>
</BODY>
</HTML>
